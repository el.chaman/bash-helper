#!/bin/bash

function msg {
  tput setf $2
  [[ ! -z "$3" ]] && tput setb "$3"
  echo -en "$1"
  tput sgr0
  echo
}

function msgTitle {
  msg "\t\t$1\t\t" 8 1
}

function msgFgTitle {
  msg "$1" 1 8
}

function msgError {
  msg "\t\t$1\t\t" 8 4
}

function msgFgError {
  msg "$1" 4 8
}

function msgInfo {
  msg "\t\t$1\t\t" 0 6
}

function msgFgInfo {
  msg "$1" 6
}

function msgSuccess {
  msg "\t\t$1\t\t" 8 2
}

function msgFgSuccess {
  msg "$1" 2 8
}

#while getopts h opt
#do
#    case "$opt" in
#        h)
#            for type in Title Error Info Success; do 
#              msg$type "This is the render of msg$type"; 
#              msgFg$type "This is the render of msgFg$type"; 
#            done
#            exit 0
#    esac
#done